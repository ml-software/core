﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MLSoftware.Core.Utilities;
using MLSoftware.Core.Logging;
using Moq;
using Serilog;
using Xunit;

namespace MLSoftware.Core.Tests
{
    public class LogConfigurationTests
    {
        [Fact]
        [Trait(TestCategories.Category, TestCategories.Manual)]
        public void SimpleLogConfigTest()
        {
            // Arrange
            var mockEnvironment = new Mock<IHostingEnvironment>();
            mockEnvironment
                .Setup(m => m.EnvironmentName)
                .Returns("UnitTest");

            var mockConfiguration = new Mock<IConfiguration>();

            LogConfiguration.ConfigureLogging(mockEnvironment.Object);

            var logger = new LoggerFactory()
                .AddSerilog(dispose: true)
                .CreateLogger(typeof(LogConfigurationTests).FullName);

            // Act
            logger.LogInformation("Starting");

            var startTime = DateTimeOffset.UtcNow;
            logger.LogInformation(1, "Started at {StartTime} and 0x{Hello:X} is hex of 42", startTime, 42);

            try
            {
                throw new Exception("Boom");
            }
            catch (Exception ex)
            {
                logger.LogCritical("Unexpected critical error starting application", ex);
                logger.Log(LogLevel.Critical, 0, "Unexpected critical error", ex, null);
                // This write should not log anything
                logger.Log<object>(LogLevel.Critical, 0, null, null, null);
                logger.LogError("Unexpected error", ex);
                logger.LogWarning("Unexpected warning", ex);
            }

            var endTime = DateTimeOffset.UtcNow;
            logger.LogInformation(2, "Stopping at {StopTime}", endTime);

            logger.LogInformation("Stopping");

            logger.LogInformation(Environment.NewLine);
            logger.LogInformation("{Result,-10:l}{StartTime,15:l}{EndTime,15:l}{Duration,15:l}", "RESULT", "START TIME", "END TIME", "DURATION(ms)");
            logger.LogInformation("{Result,-10:l}{StartTime,15:l}{EndTime,15:l}{Duration,15:l}", "------", "----- ----", "--- ----", "------------");
            logger.LogInformation("{Result,-10:l}{StartTime,15:mm:s tt}{EndTime,15:mm:s tt}{Duration,15}", "SUCCESS", startTime, endTime, (endTime - startTime).TotalMilliseconds);

            // Assert
            // Nothing to assert...need to manually check elastic search
            Log.CloseAndFlush();
        }
    }
}
