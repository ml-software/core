using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MLSoftware.Core.Data;
using MLSoftware.Core.Security;
using Moq;
using Xunit;

namespace MLSoftware.Core.Tests
{
    public class RepositoryTests
    {
        public static Guid UserId = Guid.NewGuid();
        public static Guid TenantId = Guid.NewGuid();
        public static string Username = "UnitTest";

        private StudentContext InitContext(string databaseName, IIdentityParser identityParser)
        {
            var builder = new DbContextOptionsBuilder<StudentContext>().UseInMemoryDatabase(databaseName);
            return new StudentContext(builder.Options, identityParser);
        }

        [Fact]
        public void SimpleInsert()
        {
            var databaseName = Guid.NewGuid().ToString();
            var mockIdentityParser = new Mock<IIdentityParser>();
            var user = new ApplicationUser { Id = UserId, TenantId = TenantId, Username = "UnitTest" };
            mockIdentityParser.Setup(m => m.Parse()).Returns(user);
            mockIdentityParser.Setup(m => m.TryParse(out user)).Returns(true);

            var student = new Student();
            var studentRepository = new StudentRepository(InitContext(databaseName, mockIdentityParser.Object));
            var id = studentRepository.Insert(student);

            Assert.NotNull(id);
            Assert.NotEqual(Guid.Empty, id);

            Assert.Equal(TenantId, student.TenantId);
            Assert.Equal(student.UserCreated, Username);
        }

        [Fact]
        public void InsertWithCustomId()
        {
            var databaseName = Guid.NewGuid().ToString();

            var mockIdentityParser = new Mock<IIdentityParser>();
            var user = new ApplicationUser { Id = UserId, TenantId = TenantId, Username = "UnitTest" };
            mockIdentityParser.Setup(m => m.Parse()).Returns(user);
            mockIdentityParser.Setup(m => m.TryParse(out user)).Returns(true);

            var customId = Guid.NewGuid();
            var student = new Student { Id = customId };
            var studentRepository = new StudentRepository(InitContext(databaseName, mockIdentityParser.Object));
            var id = studentRepository.Insert(student);

            Assert.NotNull(id);
            Assert.Equal(customId, id);

            Assert.Equal(TenantId, student.TenantId);
            Assert.Equal(student.UserCreated, Username);
        }

        [Fact]
        public void GetAllByTenant()
        {
            var databaseName = Guid.NewGuid().ToString();

            var mockIdentityParser1 = new Mock<IIdentityParser>();
            var user1 = new ApplicationUser { Id = Guid.NewGuid(), TenantId = Guid.NewGuid(), Username = "UnitTest1" };
            mockIdentityParser1.Setup(m => m.Parse()).Returns(user1);
            mockIdentityParser1.Setup(m => m.TryParse(out user1)).Returns(true);
            var context1 = InitContext(databaseName, mockIdentityParser1.Object);
            var studentRepository1 = new StudentRepository(context1);

            var mockIdentityParser2 = new Mock<IIdentityParser>();
            var user2 = new ApplicationUser { Id = Guid.NewGuid(), TenantId = Guid.NewGuid(), Username = "UnitTest2" };
            mockIdentityParser2.Setup(m => m.Parse()).Returns(user2);
            mockIdentityParser2.Setup(m => m.TryParse(out user2)).Returns(true);
            var context2 = InitContext(databaseName, mockIdentityParser2.Object);
            var studentRepository2 = new StudentRepository(context2);

            for (int i = 0; i < 5; i++)
            {
                studentRepository1.Insert(new Student { Firstname = $"Tenant1 {i}" });
            }

            for (int i = 0; i < 5; i++)
            {
                studentRepository2.Insert(new Student { Firstname = $"Tenant2 {i}" });
            }

            var students = studentRepository1.Get().ToList();
            Assert.Equal(5, students.Count);
            Assert.All(students, x => Assert.Equal(user1.TenantId, x.TenantId));
            Assert.All(students, x => Assert.Equal(user1.Username, x.UserCreated));
            Assert.All(students, x => Assert.StartsWith("Tenant1", x.Firstname));

            students = studentRepository2.Get().ToList();
            Assert.Equal(5, students.Count);
            Assert.All(students, x => Assert.Equal(user2.TenantId, x.TenantId));
            Assert.All(students, x => Assert.Equal(user2.Username, x.UserCreated));
            Assert.All(students, x => Assert.StartsWith("Tenant2", x.Firstname));
        }

        [Fact]
        public void SimpleGet()
        {
            var databaseName = Guid.NewGuid().ToString();

            var mockIdentityParser1 = new Mock<IIdentityParser>();
            var user1 = new ApplicationUser { Id = Guid.NewGuid(), TenantId = Guid.NewGuid(), Username = "UnitTest1" };
            mockIdentityParser1.Setup(m => m.Parse()).Returns(user1);
            mockIdentityParser1.Setup(m => m.TryParse(out user1)).Returns(true);
            var context1 = InitContext(databaseName, mockIdentityParser1.Object);
            var studentRepository1 = new StudentRepository(context1);

            var mockIdentityParser2 = new Mock<IIdentityParser>();
            var user2 = new ApplicationUser { Id = Guid.NewGuid(), TenantId = Guid.NewGuid(), Username = "UnitTest2" };
            mockIdentityParser2.Setup(m => m.Parse()).Returns(user2);
            mockIdentityParser2.Setup(m => m.TryParse(out user2)).Returns(true);
            var context2 = InitContext(databaseName, mockIdentityParser2.Object);
            var studentRepository2 = new StudentRepository(context2);

            var studentId1 = studentRepository1.Insert(new Student { Firstname = $"Tenant1" });
            var studentId2 = studentRepository2.Insert(new Student { Firstname = $"Tenant2" });

            var student = studentRepository1.Get(studentId1);
            Assert.NotNull(student);
            Assert.Equal(user1.TenantId, student.TenantId);
            student = studentRepository1.Get(studentId2);
            Assert.Null(student);

            student = studentRepository2.Get(studentId2);
            Assert.NotNull(student);
            Assert.Equal(user2.TenantId, student.TenantId);
            student = studentRepository2.Get(studentId1);
            Assert.Null(student);
        }

        [Fact]
        public void SimpleDelete()
        {
            var databaseName = Guid.NewGuid().ToString();

            var mockIdentityParser = new Mock<IIdentityParser>();
            var user = new ApplicationUser { Id = UserId, TenantId = TenantId, Username = "UnitTest" };
            mockIdentityParser.Setup(m => m.Parse()).Returns(user);
            mockIdentityParser.Setup(m => m.TryParse(out user)).Returns(true);

            var student = new Student();
            var studentRepository = new StudentRepository(InitContext(databaseName, mockIdentityParser.Object));
            var id = studentRepository.Insert(student);

            var students = studentRepository.Get().ToList();
            Assert.Equal(1, students.Count);

            studentRepository.Delete(id);

            students = studentRepository.Get().ToList();
            Assert.Equal(0, students.Count);
        }

        [Fact]
        public void SimpleUpdate()
        {
            var databaseName = Guid.NewGuid().ToString();

            var mockIdentityParser = new Mock<IIdentityParser>();
            var user = new ApplicationUser { Id = Guid.NewGuid(), TenantId = Guid.NewGuid(), Username = "UnitTest1" };
            mockIdentityParser.Setup(m => m.Parse()).Returns(user);
            mockIdentityParser.Setup(m => m.TryParse(out user)).Returns(true);

            Guid studentId1;

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser.Object)))
            {
                studentId1 = repository.Insert(new Student { Firstname = "Tenant1" });
                Assert.NotEqual(Guid.Empty, studentId1);
            }

            Student student;

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser.Object)))
            {
                student = repository.Get(studentId1);
                Assert.NotNull(student);
                Assert.Equal("Tenant1", student.Firstname);
                Assert.Equal(user.Username, student.UserCreated);
                Assert.True(student.Created > DateTime.UtcNow.AddMinutes(-1));
            }

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser.Object)))
            {
                student.Firstname = "Update";
                repository.Update(student);
            }

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser.Object)))
            {
                student = repository.Get(studentId1);
                Assert.NotNull(student);
                Assert.Equal("Update", student.Firstname);
                Assert.Equal(user.Username, student.UserCreated);

                Assert.Equal(user.Username, student.UserModified);
                Assert.True(student.Modified > DateTime.UtcNow.AddMinutes(-1));
            }
        }

        [Fact]
        public void CrossTenantUpdate()
        {
            var databaseName = Guid.NewGuid().ToString();

            var mockIdentityParser1 = new Mock<IIdentityParser>();
            var user1 = new ApplicationUser { Id = Guid.NewGuid(), TenantId = Guid.NewGuid(), Username = "UnitTest1" };
            mockIdentityParser1.Setup(m => m.Parse()).Returns(user1);
            mockIdentityParser1.Setup(m => m.TryParse(out user1)).Returns(true);

            var mockIdentityParser2 = new Mock<IIdentityParser>();
            var user2 = new ApplicationUser { Id = Guid.NewGuid(), TenantId = Guid.NewGuid(), Username = "UnitTest2" };
            mockIdentityParser2.Setup(m => m.Parse()).Returns(user2);
            mockIdentityParser2.Setup(m => m.TryParse(out user2)).Returns(true);

            Guid studentId1;
            Guid studentId2;

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser1.Object)))
            {
                studentId1 = repository.Insert(new Student { Firstname = "Tenant1" });
                Assert.NotEqual(Guid.Empty, studentId1);
            }

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser2.Object)))
            {
                studentId2 = repository.Insert(new Student { Firstname = "Tenant2" });
                Assert.NotEqual(Guid.Empty, studentId2);
            }

            Student student;

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser2.Object)))
            {
                student = repository.Get(studentId2);
                Assert.NotNull(student);
                Assert.NotEqual(Guid.Empty, student.Id);
                Assert.Equal(studentId2, student.Id);
                Assert.Equal(user2.TenantId, student.TenantId);
            }

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser2.Object)))
            {
                student.TenantId = user1.TenantId;
                Assert.Throws<CrossTenantException>(() => repository.Update(student));
            }
        }

        [Fact]
        public void CrossTenantDelete()
        {
            var databaseName = Guid.NewGuid().ToString();

            var mockIdentityParser1 = new Mock<IIdentityParser>();
            var user1 = new ApplicationUser { Id = Guid.NewGuid(), TenantId = Guid.NewGuid(), Username = "UnitTest1" };
            mockIdentityParser1.Setup(m => m.Parse()).Returns(user1);
            mockIdentityParser1.Setup(m => m.TryParse(out user1)).Returns(true);

            var mockIdentityParser2 = new Mock<IIdentityParser>();
            var user2 = new ApplicationUser { Id = Guid.NewGuid(), TenantId = Guid.NewGuid(), Username = "UnitTest2" };
            mockIdentityParser2.Setup(m => m.Parse()).Returns(user2);
            mockIdentityParser2.Setup(m => m.TryParse(out user2)).Returns(true);

            Guid studentId1;
            Guid studentId2;

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser1.Object)))
            {
                studentId1 = repository.Insert(new Student { Firstname = "Tenant1" });
                Assert.NotEqual(Guid.Empty, studentId1);
            }

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser2.Object)))
            {
                studentId2 = repository.Insert(new Student { Firstname = "Tenant2" });
                Assert.NotEqual(Guid.Empty, studentId2);
            }

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser1.Object)))
            {
                Assert.Equal(1, repository.Get().ToList().Count);
                repository.Delete(studentId2);
                Assert.Equal(1, repository.Get().ToList().Count);
                repository.Delete(studentId1);
                Assert.Equal(0, repository.Get().ToList().Count);
            }

            using (var repository = new StudentRepository(InitContext(databaseName, mockIdentityParser2.Object)))
            {
                Assert.Equal(1, repository.Get().ToList().Count);
                repository.Delete(studentId1);
                Assert.Equal(1, repository.Get().ToList().Count);
                repository.Delete(studentId2);
                Assert.Equal(0, repository.Get().ToList().Count);
            }
        }
    }

    public class StudentRepository : Repository<StudentContext, Student>, IDisposable
    {
        public StudentRepository(StudentContext context) : base(context)
        {
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _context?.Dispose();
                }

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~StudentRepository() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }

    public class StudentContext : ApplicationDbContextBase
    {
        public DbSet<Student> Students { get; set; }

        public StudentContext(DbContextOptions<StudentContext> options, IIdentityParser identityParser)
        : base(options, identityParser) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.SetGlobalQuery<Student>(modelBuilder);
            base.OnModelCreating(modelBuilder);
        }
    }

    public class Student : EntityBase
    {
        public string Firstname { get; set; }
    }
}