# MLSoftware.Core

[![MyGet CI](https://img.shields.io/myget/mlsoftware/v/MLSoftware.Core.svg)](http://myget.org/gallery/ml-software)

## Description

This base library contains base classes and helper methods that can be used by any MLSoftware project.

## Where can I get it?

Get it from MyGet. You can simply install it with the Package Manager console:

    PM> Install-Package MLSoftware.Core

## Workflow

This repositories uses the GitHubFlow for branching and releasing. Also for versioning we are using the `Mainline development` mode which is decribed [here](http://gitversion.readthedocs.io/en/latest/reference/mainline-development/).

Create a new `feature` branch from `master` and use a pull-request to merge it back.