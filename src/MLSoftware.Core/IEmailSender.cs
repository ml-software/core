﻿using System.Threading.Tasks;

namespace MLSoftware.Core
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
