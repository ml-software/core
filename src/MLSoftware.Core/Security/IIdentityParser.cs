namespace MLSoftware.Core.Security
{
    public interface IIdentityParser
    {
        bool TryParse(out ApplicationUser user);
        ApplicationUser Parse();
    }
}