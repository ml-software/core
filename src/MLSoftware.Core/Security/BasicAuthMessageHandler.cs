using System;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace MLSoftware.Core.Security
{
    public class BasicAuthMessageHandler : HttpClientHandler
    {
        private readonly string _username;
        private readonly string _password;
        public BasicAuthMessageHandler(string username, string password)
        {
            _username = username;
            _password = password;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            request.Headers.Add("Authorization", "Basic " + Convert.ToBase64String(System.Text.ASCIIEncoding.ASCII.GetBytes($"{_username}:{_password}")));
            return await base.SendAsync(request, cancellationToken);
        }
    }
}