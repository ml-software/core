﻿using System;

namespace MLSoftware.Core.Security
{
    public class ApplicationUser
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
