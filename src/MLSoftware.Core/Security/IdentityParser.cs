﻿using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace MLSoftware.Core.Security
{
    public class IdentityParser : IIdentityParser
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public IdentityParser(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public ApplicationUser Parse()
        {
            if (TryParse(out ApplicationUser user))
            {
                return user;
            }

            throw new ApplicationException("Unable to parse user");
        }

        public bool TryParse(out ApplicationUser user)
        {
            user = null;

            var success = false;

            try
            {
                var principal = _httpContextAccessor?.HttpContext?.User;

                // Pattern matching 'is' expression
                // assigns "claims" if "principal" is a "ClaimsPrincipal"
                if (principal is ClaimsPrincipal claims)
                {
                    Guid.TryParse(claims.Claims.FirstOrDefault(x => x.Type == "extension_TenantId")?.Value, out Guid tenantId);

                    user = new ApplicationUser
                    {
                        Id = Guid.Parse(claims.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier)?.Value ?? Guid.Empty.ToString()),
                        FirstName = claims.Claims.FirstOrDefault(x => x.Type == ClaimTypes.GivenName)?.Value ?? "",
                        LastName = claims.Claims.FirstOrDefault(x => x.Type == ClaimTypes.Surname)?.Value ?? "",
                        Username = claims.Claims.FirstOrDefault(x => x.Type == "name")?.Value ?? "",
                        Email = claims.Claims.FirstOrDefault(x => x.Type == "emails")?.Value ?? "",
                        TenantId = tenantId
                    };

                    success = true;
                }
            }
            catch
            {
                // ToDo: Log Error
            }

            return success;
        }
    }
}
