﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MLSoftware.Core
{
    public abstract class EntityViewModelBase
    {
        [Key]
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }
        public string UserCreated { get; set; }
        public string UserModified { get; set; }

        [NotMapped]
        public bool IsModified => Modified != null;
    }
}
