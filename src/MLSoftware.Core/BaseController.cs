﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MLSoftware.Core.Security;

namespace MLSoftware.Core
{
    [Authorize]
    [Route("[controller]")]
    public abstract class BaseController : Controller
    {
        protected ApplicationUser _currentUser;

        public BaseController(IIdentityParser identityParser)
        {
            if(identityParser.TryParse(out ApplicationUser user))
            {
                _currentUser = user;
            }
        }
    }
}
