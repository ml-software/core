using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MimeKit;
using MailKit.Net.Smtp;
using MimeKit.Text;

namespace MLSoftware.Core
{
    public class DebugIoMailSender : IEmailSender
    {
        private readonly MailOptions _mailOptions;
        private readonly ILogger<DebugIoMailSender> _logger;

        public DebugIoMailSender(IOptions<MailOptions> mailOptions, ILogger<DebugIoMailSender> logger)
        {
            _mailOptions = mailOptions.Value;
            _logger = logger;
        }

        public async Task SendEmailAsync(string email, string subject, string message)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new ArgumentNullException(nameof(email));
            }

            if (string.IsNullOrEmpty(subject))
            {
                throw new ArgumentNullException(nameof(subject));
            }

            if (string.IsNullOrEmpty(message))
            {
                throw new ArgumentNullException(nameof(message));
            }

            _logger.LogInformation("Sending email to {To} with the subject {Subject} message: {Message}", email, subject, message);

            var mimeMessage = new MimeMessage();
            mimeMessage.From.Add(new MailboxAddress(_mailOptions.From, _mailOptions.Login));
            mimeMessage.To.Add(new MailboxAddress(email));
            mimeMessage.Subject = subject;

            mimeMessage.Body = new TextPart(TextFormat.Html)
            {
                Text = message
            };

            using (var client = new SmtpClient())
            {
                // For demo-purposes, accept all SSL certificates (in case the server supports STARTTLS)
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;

                await client.ConnectAsync(_mailOptions.Host, _mailOptions.Port, false);

                // Note: since we don't have an OAuth2 token, disable
                // the XOAUTH2 authentication mechanism.
                client.AuthenticationMechanisms.Remove("XOAUTH2");

                // Note: only needed if the SMTP server requires authentication
                await client.AuthenticateAsync(_mailOptions.Login, _mailOptions.Password);

                await client.SendAsync(mimeMessage);
                await client.DisconnectAsync(true);
            }
        }
    }
}