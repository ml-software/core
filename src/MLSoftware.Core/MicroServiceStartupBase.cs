﻿using CorrelationId;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MLSoftware.Core.Logging;
using MLSoftware.Core.Security;
using MLSoftware.Core.Utilities;
using Serilog;

namespace MLSoftware.Core
{
    public abstract class MicroServiceStartupBase
    {
        public IConfiguration Configuration { get; }

        public MicroServiceStartupBase(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        protected virtual void ConfigureLogging(IHostingEnvironment env)
        {
            var loggerConfiguration = LogConfiguration
                            .ConfigureLogging(env)
                            .WriteToLiterateConsole()
                            .WriteToSeq(serverUrl: Configuration["Logging:Seq:Url"], username: Configuration["Logging:Seq:BasicAuthUsername"], password: Configuration["Logging:Seq:BasicAuthPassword"])
                            .WriteToSlack(webhookUrl: Configuration["Logging:Slack:WebhookUrl"]);

            Log.Logger = loggerConfiguration.CreateLogger();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public virtual void ConfigureServices(IServiceCollection services)
        {
            // Add framework services
            services.AddMvc();

            // Add functionality to inject IOptions<T>
            services.AddOptions();
            services.Configure<AzureAdB2COptions>(Configuration.GetSection("Authentication:AzureAdB2C"));
            services.Configure<MailOptions>(Configuration.GetSection("MailOptions"));

            ConfigureDependencyInjection(services);
        }

        public virtual void ConfigureDependencyInjection(IServiceCollection services)
        {
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddScoped<IHttpClient, StandardHttpClient>();

            services.AddTransient<IEmailSender, DebugIoMailSender>();
            services.AddTransient<IIdentityParser, IdentityParser>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseCorrelationId();

            ConfigureLogging(env);

            // Add Serilog to the logging pipeline
            loggerFactory.AddSerilog();

            app.UseMiddleware<SerilogMiddleware>();

            app.UseMvc();
        }
    }
}
