using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.Identity.Client;
using MLSoftware.Core.Security;
using Newtonsoft.Json;

namespace MLSoftware.Core
{
    public abstract class ServiceBase
    {
        private readonly AzureAdB2COptions _azureOptions;
        private readonly IHttpClient _httpClient;
        private readonly IIdentityParser _identityParser;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ILogger<ServiceBase> _logger;

        private HttpContext _httpContext => _httpContextAccessor?.HttpContext;

        public ServiceBase(
            IOptions<AzureAdB2COptions> azureOptions, 
            IHttpClient httpClient, 
            IIdentityParser identityParser, 
            IHttpContextAccessor httpContextAccessor,
            ILogger<ServiceBase> logger)
        {
            _azureOptions = azureOptions.Value;
            _httpClient = httpClient;
            _identityParser = identityParser;
            _httpContextAccessor = httpContextAccessor;
            _logger = logger;
        }

        protected async Task Post(string uri, object entity)
        {
            var response = await _httpClient.PostAsync(uri, entity, authorizationToken: await GetUserToken());

            response.EnsureSuccessStatusCode();
        }

        protected async Task<T> Post<T>(string uri, object data)
        {
            var response = await _httpClient.PostAsync(uri, data, authorizationToken: await GetUserToken());

            response.EnsureSuccessStatusCode();
            var dataString = await response.Content.ReadAsStringAsync();

            var responseObject = JsonConvert.DeserializeObject<T>(dataString);
            return responseObject;
        }

        protected async Task Put(string uri, object entity)
        {
            var response = await _httpClient.PutAsync(uri, entity, authorizationToken: await GetUserToken());

            response.EnsureSuccessStatusCode();
        }

        protected async Task<T> Get<T>(string uri)
        {
            var response = await _httpClient.GetAsync(uri, authorizationToken: await GetUserToken());

            response.EnsureSuccessStatusCode();
            var dataString = await response.Content.ReadAsStringAsync();

            var responseObject = JsonConvert.DeserializeObject<T>(dataString);
            return responseObject;
        }

        protected async Task Delete(string uri)
        {
            var response = await _httpClient.DeleteAsync(uri, authorizationToken: await GetUserToken());
            response.EnsureSuccessStatusCode();
        }

        protected virtual async Task<string> GetUserToken()
        {
            var signedInUserId = string.Empty;

            if(_identityParser.TryParse(out ApplicationUser user))
            {
                signedInUserId = user.Id.ToString();
            }

            var userTokenCache = new MSALSessionCache(signedInUserId, _httpContext).GetMsalCacheInstance();

            var cca = new ConfidentialClientApplication(
                _azureOptions.ClientId,
                _azureOptions.Authority,
                "https://apis/",
                new ClientCredential(_azureOptions.ClientSecret),
                userTokenCache,
                null);

            try
            {
                var result = await cca.AcquireTokenSilentAsync(_azureOptions.Scopes.Split(' '), cca.Users.FirstOrDefault(), _azureOptions.Authority, false);
                return result.AccessToken;
            }
            catch(Exception exception)
            {
                _logger.LogError(exception, "Failed to aquire token for user {userId}", signedInUserId);
                return null;
            }
        }
    }
}