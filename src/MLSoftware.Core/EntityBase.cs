﻿using System;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace MLSoftware.Core
{
    public abstract class EntityBase
    {
        public Guid Id { get; set; }
        public Guid TenantId { get; set; }
        public DateTime Created { get; set; }
        public DateTime? Modified { get; set; }

        [MaxLength(20)]
        public string UserCreated { get; set; }

        [MaxLength(20)]
        public string UserModified { get; set; }

        [JsonIgnore]
        public bool Deleted { get; set; }
    }
}
