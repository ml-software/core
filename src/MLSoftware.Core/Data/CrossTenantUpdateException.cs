using System;
using System.Collections.Generic;

namespace MLSoftware.Core.Data
{
    public class CrossTenantException : ApplicationException
    {
        public IList<Guid> TenantIds { get; private set; }

        public CrossTenantException(IList<Guid> tenantIds)
        {
            TenantIds = tenantIds;
        }
    }
}