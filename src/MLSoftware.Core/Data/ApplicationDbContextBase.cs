using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyModel;
using MLSoftware.Core.Security;

namespace MLSoftware.Core.Data
{
    public abstract class ApplicationDbContextBase : DbContext
    {
        private readonly IIdentityParser _identityParser;
        private Guid _tenantId = Guid.Empty;

        public ApplicationDbContextBase()
        {
        }

        public ApplicationDbContextBase(DbContextOptions options)
            : base(options)
        {
        }

        public ApplicationDbContextBase(DbContextOptions options, IIdentityParser identityParser)
            : base(options)
        {
            _identityParser = identityParser;
            if (_identityParser != null && _identityParser.TryParse(out ApplicationUser user))
            {
                _tenantId = user.TenantId;
            }
        }

        public override int SaveChanges()
        {
            UpdateProperties();
            ThrowIfMultipleTenants();
            return base.SaveChanges();
        }

        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            UpdateProperties();
            ThrowIfMultipleTenants();
            return await base.SaveChangesAsync(cancellationToken);
        }

        public void SetGlobalQuery<T>(ModelBuilder builder) where T : EntityBase
        {
            builder.Entity<T>().HasKey(e => e.Id);
            builder.Entity<T>().HasQueryFilter(e => _tenantId == Guid.Empty ? true : e.TenantId == _tenantId);
        }

        private void UpdateProperties()
        {
            var username = "System";

            if (_identityParser != null && _identityParser.TryParse(out ApplicationUser user))
            {
                username = user.Username;
            }

            var addedSourceInfo =
                ChangeTracker.Entries()
                    .Where(e => e.State == EntityState.Added);

            foreach (var entry in addedSourceInfo)
            {
                if ((Guid)entry.Property("Id").CurrentValue == Guid.Empty)
                {
                    entry.Property("Id").CurrentValue = Guid.NewGuid();
                }

                // Quick Fix
                try
                {
                    entry.Property("TenantId").CurrentValue = _tenantId;
                }
                catch { }
                //

                entry.Property("Created").CurrentValue = DateTime.UtcNow;
                entry.Property("UserCreated").CurrentValue = username;
            }

            var modifiedSourceInfo =
                ChangeTracker.Entries()
                    .Where(e => e.State == EntityState.Modified);

            foreach (var entry in modifiedSourceInfo)
            {
                entry.Property("Modified").CurrentValue = DateTime.UtcNow;
                entry.Property("UserModified").CurrentValue = username;
            }
        }

        private void ThrowIfMultipleTenants()
        {
            var ids = (from e in ChangeTracker.Entries()
                       where e.Entity is EntityBase
                       select ((EntityBase)e.Entity).TenantId)
                        .Distinct()
                        .ToList();

            if (ids.Count == 0)
            {
                return;
            }

            if (ids.Count > 1)
            {
                throw new CrossTenantException(ids);
            }

            if (_tenantId != Guid.Empty)
            {
                if (ids.Any(x => x != _tenantId))
                {
                    throw new CrossTenantException(ids);
                }
            }
        }
    }
}