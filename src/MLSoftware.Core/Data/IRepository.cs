﻿using System;
using System.Linq;

namespace MLSoftware.Core.Data
{
    public interface IRepository<T> where T : EntityBase
    {
        IQueryable<T> Get();
        T Get(Guid id);
        Guid Insert(T entity);
        void Update(T entity);
        void Delete(Guid id);
    }
}
