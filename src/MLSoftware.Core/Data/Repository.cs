﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace MLSoftware.Core.Data
{
    public class Repository<TContext, T> : IRepository<T>
        where T : EntityBase
        where TContext : ApplicationDbContextBase
    {
        protected readonly TContext _context;
        protected DbSet<T> _entities;

        public Repository(TContext context)
        {
            _context = context;
            _entities = _context.Set<T>();
        }

        public virtual void Delete(Guid id)
        {
            var entity = Get(id);

            if (entity != null)
            {
                entity.Deleted = true;
                _context.SaveChanges();
            }
        }

        public virtual IQueryable<T> Get()
        {
            return _entities.Where(x => !x.Deleted);
        }

        public virtual T Get(Guid id)
        {
            return _entities.SingleOrDefault(x => x.Id == id && !x.Deleted);
        }

        public virtual Guid Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            _entities.Add(entity);
            _context.SaveChanges();

            return entity.Id;
        }

        public virtual void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }

            _context.Entry(entity).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
