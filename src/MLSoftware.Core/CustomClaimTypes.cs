﻿namespace MLSoftware.Core
{
    public static class CustomClaimTypes
    {
        private const string TridooBase = "https://tridoo.ml-software.ch";

        public static string HotelId = $"{TridooBase}/hotelid";
        public static string Nickname = "nickname";
        public static string Picture = "picture";
    }
}
