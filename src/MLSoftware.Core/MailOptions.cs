namespace MLSoftware.Core
{
    public class MailOptions
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string From { get; set; }
    }
}