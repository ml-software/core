using System.Threading.Tasks;

namespace MLSoftware.Core
{
    public interface IViewRenderer
    {
        Task<string> RenderViewAsync(string viewName, object model);
    }
}