﻿using System;
using System.Net.Http;
using Microsoft.AspNetCore.Hosting;
using MLSoftware.Core.Security;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Sinks.Elasticsearch;
using Serilog.Sinks.Slack;

namespace MLSoftware.Core.Logging
{
    public static class LogConfiguration
    {
        public static LoggerConfiguration ConfigureLogging(IHostingEnvironment env)
        {
            return new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .MinimumLevel.Override("System", LogEventLevel.Warning)
                .Enrich.WithProperty("ApplicationName", env.ApplicationName)
                .Enrich.WithProperty("EnvironmentName", env.EnvironmentName)
                .Enrich.WithProperty("SourceContext", "")
                .Enrich.FromLogContext();
        }

        public static LoggerConfiguration WriteToLiterateConsole(this LoggerConfiguration loggerConfiguration, LogEventLevel restrictedToMinimumLevel = LogEventLevel.Verbose)
        {
            loggerConfiguration.WriteTo.LiterateConsole(restrictedToMinimumLevel: restrictedToMinimumLevel);
            return loggerConfiguration;
        }

        public static LoggerConfiguration WriteToRollingFile(this LoggerConfiguration loggerConfiguration, string filePath, LogEventLevel restrictedToMinimumLevel = LogEventLevel.Error)
        {
            loggerConfiguration.WriteTo.RollingFile(filePath, restrictedToMinimumLevel);
            return loggerConfiguration;
        }

        public static LoggerConfiguration WriteToSlack(this LoggerConfiguration loggerConfiguration, string webhookUrl, string channel = "#logs", LogEventLevel restrictedToMinimumLevel = LogEventLevel.Error)
        {
            if (!channel.StartsWith("#"))
            {
                channel = $"#{channel}";
            }

            var options = new SlackSinkOptions
            {
                WebHookUrl = webhookUrl,
                CustomChannel = channel
            };

            loggerConfiguration.WriteTo.Slack(options, restrictedToMinimumLevel: restrictedToMinimumLevel);

            return loggerConfiguration;
        }

        public static LoggerConfiguration WriteToElasticsearch(this LoggerConfiguration loggerConfiguration, string serverUrl, string indexFormat = "logstash-{0:yyyy.MM.dd}")
        {
            var options = new ElasticsearchSinkOptions(new Uri(serverUrl))
            {
                AutoRegisterTemplate = true,
                IndexFormat = indexFormat
                //ModifyConnectionSettings = (c) => c.GlobalHeaders
            };

            loggerConfiguration.WriteTo.Elasticsearch(options);

            return loggerConfiguration;
        }

        public static LoggerConfiguration WriteToSeq(this LoggerConfiguration loggerConfiguration, string serverUrl, string apiKey = null, string username = null, string password = null, LogEventLevel restrictedToMinimumLevel = LogEventLevel.Verbose)
        {
            LoggingLevelSwitch levelSwitch = null;

            if (!string.IsNullOrEmpty(apiKey))
            {
                levelSwitch = new LoggingLevelSwitch();
            }

            HttpMessageHandler messageHandler = null;

            if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
            {
                messageHandler = new BasicAuthMessageHandler(username, password);
            }

            loggerConfiguration.WriteTo.Seq(serverUrl, apiKey: apiKey, messageHandler: messageHandler, controlLevelSwitch: levelSwitch, restrictedToMinimumLevel: restrictedToMinimumLevel);

            return loggerConfiguration;
        }
    }
}
