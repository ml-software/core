using System.Linq;
using Serilog;

namespace MLSoftware.Core.Logging
{
    public static class LoggingExtensions
    {
        ///<example>
        /// log.InformationEvent("NewUser", "{Email}", user.Email);
        ///</example>
        public static void InformationEvent(this ILogger logger, string eventId, string messageTemplate, params object[] propertyValues)
        {
            var allProps = new object[] { eventId }.Concat(propertyValues).ToArray();
            logger.Information("<{EventID:l}> " + messageTemplate, allProps);
        }

        ///<example>
        /// log.With("User", user)
        ///    .InformationEvent("NewUser", "{Email}", user.email)
        ///</example>
        public static ILogger With(this ILogger logger, string propertyName, object value, bool destructureObjects = false)
        {
            return logger.ForContext(propertyName, value, destructureObjects);
        }
    }
}